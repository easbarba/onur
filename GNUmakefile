# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

# DEPS: gcc meson ninja muon coreutils valgrind indent splint cunit

.DEFAULT_GOAL: test

PREFIX     ?= ${HOME}/.local/bin
NAME       = onur
RM         = rm -rf
BUILDDIR = ./build

RUNNER := podman
VERSION := $(shell awk -F " " '/onur VERSION/ { print $$4; exit }' CMakeLists.txt)
CONTAINER_IMAGE := registry.gitlab.com/${USER}/${NAME}:${VERSION}

# ------------------------------------ LOCAL

.PHONY: local.all
local.all: local.cmake.all

.PHONY: local.dev
local.dev: local.cmake.dev.setup local.cmake.dev.build

.PHONY: local.cmake.all
local.cmake.all: local.clean
	cmake -S . -B ${BUILDDIR} -D CMAKE_BUILD_TYPE=Release --fresh -Wno-dev
	cmake --build ${BUILDDIR}

.PHONY: local.cmake.dev.setup
local.cmake.dev.setup:
	cmake -S . -B ${BUILDDIR} -D CMAKE_BUILD_TYPE=Debug

.PHONY: local.cmake.dev.build
local.cmake.dev.build:
	cmake --build ${BUILDDIR}

.PHONY: local.xmake
local.xmake:
	xmake

.PHONY: local.bazel.build
local.bazel.build:
	 bazel build //:onur_main

.PHONY: local.bazel.install
local.bazel.install:
	 cp -v ./bazel-bin/onur_main ${PREFIX}/onur

.PHONY: local.meson.all
local.meson.all:
	CC=g++ meson setup $(BUILDDIR) --wipe
	CC=g++ meson compile -C $(BUILDDIR)

.PHONY: local.meson.dev
local.meson.dev:
	CC=g++ meson setup $(BUILDDIR)
	CC=g++ meson compile -C $(BUILDDIR)

.PHONY: local.clean
local.clean:
	@$(RM) $(BUILDDIR)

.PHONY: local.install
local.install:
	cp -v ${BUILDDIR}/src/onur ${PREFIX}/onur

.PHONY: local.uninstall
local.uninstall:
	rm ${PREFIX}/${NAME}

.PHONY: local.format
local.format:
	clang-format -i ./src/*.cpp ./include/*.hpp

.PHONY: local.lint
local.lint:
	clang-check ./src/*.cpp

.PHONY: local.leaks
local.leaks:
	valgrind --leak-check=full \
         --show-leak-kinds=all \
         --track-origins=yes \
         --verbose \
         ./build/onur grab
         # --log-file=valgrind-output \

.PHONY: local.run
local.run: local.cmake.dev.build
	${BUILDDIR}/src/onur grab

.PHONY: test
test: image.test
	 # ./${BUILDDIR}/tests/onur_tests
	 # ctest --test-dir ${BUILDDIR}/tests

# ------------------------------- CONTAINER

.PHONY: image.build.cmake
image.build.cmake:
	${RUNNER} build \
		--file ./Containerfile.cmake \
		--tag ${CONTAINER_IMAGE} \
		--env ONUR_VERSION=${VERSION} \
		--no-cache

.PHONY: image.build.bazel
image.build.bazel:
	${RUNNER} build \
		--file ./Containerfile.bazel \
		--tag ${CONTAINER_IMAGE} \
		--env ONUR_VERSION=${VERSION} \
		--no-cache

.PHONY: image.repl
image.repl:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /home/easbarba/app \
		${CONTAINER_IMAGE} bash

.PHONY: image.publish
image.publish:
	${RUNNER} push ${CONTAINER_IMAGE}

.PHONY: image.test
image.test:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		${CONTAINER_IMAGE} # bash -c "meson test -C build"
		# --workdir /home/easbarba/app \

.PHONY: image.commands
image.commands:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /home/easbarba/app \
		${CONTAINER_IMAGE} bash -c "$(shell cat ./container-commands | fzf)"
