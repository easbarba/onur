/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

// #define CATCH_CONFIG_MAIN
#include <catch2/catch_all.hpp>

#include "../src/include/globals.hpp"

TEST_CASE ("Projects directories", "[dirs]")
{
  SECTION ("USER HOME directory")
  {
    Globals globals;
    REQUIRE (globals.home_dir == "/root");
  }

  SECTION ("Onur directory")
  {
    Globals globals;
    REQUIRE (globals.onur_dir == "/root/.config/onur");
  }

  SECTION ("Projects directory")
  {
    Globals globals;
    REQUIRE (globals.projects_dir == "/root/Projects");
  }
}
