FROM debian:unstable

MAINTAINER EAS Barbosa <easbarba@outlook.com>
LABEL version=${ONUR_VERSION}
LABEL description="Easily manage multiple FLOSS repositories."

RUN apt-get update && \
        apt-get install -y --no-install-recommends build-essential git ca-certificates cmake rapidjson-dev && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*


# meson pkg-config
# ENV USERNAME easbarba
# ENV APP_HOME /home/$USERNAME/app
# RUN groupadd -r $USERNAME && useradd -r -g $USERNAME -d /home/$USERNAME -m -s /bin/bash $USERNAME
# RUN chown -R $USERNAME:$USERNAME /home/$USERNAME
# WORKDIR $APP_HOME

WORKDIR /app

COPY examples examples
COPY ./prepare.bash .
RUN ./prepare.bash

COPY . .

ENV BUILDDIR /app/build
RUN cmake -S . -B $BUILDDIR -D CMAKE_BUILD_TYPE=Debug
RUN cmake --build $BUILDDIR

CMD [ "/app/build/tests/onur_tests" ]
