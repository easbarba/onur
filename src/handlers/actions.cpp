/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <format>
#include <string>

#include "../include/actions.hpp"

using std::format;
using std::string;
using std::system;
using std::filesystem::path;

Actions::Actions () {}

auto
Actions::klone (Project project, path dir_path) -> void
{
  auto final_command{ format ("git clone --single-branch --branch={} "
                              "--depth=1 --quiet --no-tags -- {} {}",
                              project.branch, project.url,
                              dir_path.string ()) };
  system (final_command.c_str ());
}

auto
Actions::pull (Project project, path dir_path) -> void
{
  auto final_command{ format (
      "git -C {} fetch --no-tags --depth=1 --force --prune --quiet origin {}",
      dir_path.string (), project.branch) };
  auto clean_command{ format ("git -C {} reset --hard --quiet",
                              dir_path.string ()) };
  system (final_command.c_str ());
}
