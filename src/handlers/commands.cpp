/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <filesystem>
#include <format>
#include <optional>
#include <print>
#include <string>

#include "../include/commands.hpp"
#include "../include/helpers.hpp"

using std::filesystem::exists;
using std::filesystem::path;

Commands::Commands () {}

auto
Commands::grab (std::optional<std::string> name) -> void
{
  // if (name.has_value ())
  //   ConfigTopic config{ configTopicNew (name.value ()) };

  for (auto single_config : parse.multi ())
    {
      if (name.has_value () && name.value () != single_config.name)
        continue;

      std::println ("{}: ", single_config.name);

      for (auto topic : single_config.topics)
        {
          std::println ("  + {}", topic.first);
          for (auto project : topic.second)
            {
              auto final_path{ path (globals.projects_dir / single_config.name
                                     / topic.first / project.name) };

              print_project_info (project);

              if (exists (final_path / ".git" / "config"))
                actions.pull (project, final_path);
              else
                actions.klone (project, final_path);
            }

          std::println ();
        }
    }
}

auto
Commands::backup (void) -> void
{
  std::println ("Backing up");
}

ConfigTopic
Commands::config_topic_new (std::string &name)
{
  ConfigTopic config;

  if (name.contains ("."))
    {
      config.dot = { true };

      std::size_t dot_positon{ name.find (".") };
      config.name = { name.substr (0, dot_positon) };
      config.topic = { name.substr (dot_positon + 1) };

      return config;
    }

  config.name = name;
  return config;
}

auto
Commands::config (std::string name, ConfigEntries entries) -> void
{
  ConfigTopic config{ config_topic_new (name) };
  // std::println ("{}", config.dot);
  // return;
  // if (!config.name.has_value ())
  // return;

  // config by name not found
  // if (!parse.exist (config.name.value ()))
  //   {
  //     std::println ("No configuration by {} found!", config.name.value ());
  //     return;
  //   }

  // list topics of config
  if (config.dot)
    {
      print_single_config (config, true);
      return;
    }

  // list all projects of config
  if (!config.topic.has_value ())
    {
      print_single_config (config);
      return;
    }
  else // list projects of topic in config
    {
      print_single_config (config);
      return;
    }

  // std::println (
  //     "Please provide a topic and entries to save a new one. Exiting!",
  //     config.name);

  if (!entries.name.has_value () || !entries.url.has_value ())
    {
      std::println ("Enter name and url entries at least. Exiting!");
      return;
    }

  parse.save (config.name.value (), config.topic.value (), entries);
}

void
Commands::print_single_config (ConfigTopic config, bool onlytopics)
{
  for (auto single_config : parse.multi ())
    {
      if (config.name.value () != single_config.name)
        continue;

      std::println ("{}:", single_config.name);
      for (auto topic : single_config.topics)
        {
          if (onlytopics)
            {
              std::print ("  {} ", topic.first);
              continue;
            }

          if (config.topic.has_value ()
              && config.topic.value () != topic.first)
            continue;

          std::println (" + {}", topic.first);

          for (auto project : topic.second)
            print_project_info (project);
        }
    }
}
