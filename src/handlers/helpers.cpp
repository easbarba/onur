/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <format>
#include <print>

#include "../include/helpers.hpp"

using namespace std;

auto
print_project_info (Project project) -> void
{
  std::string::size_type name_length = 27;
  auto name_truncated
      = project.name.length () <= name_length
            ? project.name
            : project.name.substr (0, name_length).append ("...");
  std::string::size_type url_length = 60;
  auto url_truncated = project.url.length () <= url_length
                           ? project.url
                           : project.url.substr (0, url_length).append ("...");
  std::println ("{:4}- {:35} {:75} {}", "", name_truncated, url_truncated,
                project.branch);
}
